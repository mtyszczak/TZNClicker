/**
 * playing.ts - playing stage implementation
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { Difficulty, Example } from "../../../assets/highlight";
import { Speed } from "../../../../libraries/typeore/src/assets/speed";
import { UI, LineOrientation } from "../../../../libraries/typeore/src/assets/ui";
import { Editor, EditorState } from "../editor";
import { GameState, GameStateObject } from "../../../../libraries/typeore/src/game_state";
import { TZNClicker } from "../../../tznclicker";
import { Stage, StageState } from "../../../../libraries/typeore/src/graphics/drawable/stage/stage";
import { staticImplements } from "../../../../libraries/js-helpers/src/utils";
import { AllInteractiveEvents, SupportedEventTypes } from "../../../../libraries/typeore/src/graphics/drawable/interactive/interactive";
import { ContextPanel } from "../../../../libraries/typeore/src/graphics/drawable/context_panel";
import { Rectangle } from "../../../../libraries/typeore/src/graphics/drawable/shape/rectangle";

/**
 * Type of the Playing stage behaviour
 * @enum
 */
export enum PlayingType {
  /**
   * Plays the game for the player (Used as an animation in the title screen)
   */
  AUTO,
  /**
   * Increments y offset only when user typed the entire line
   */
  ZEN,
  /**
   * Normal gameplay with offset y being automatically incremented depending on difficulty
   */
  NORMAL
}

/**
 * Playing stage implementation
 * @class
 * @implements Stage
 * @implements StageState
 */
 @staticImplements< StageState >()
export class Playing implements Stage {

  /**
   * GameState corresponding to this stage
   * @type {GameStateObject}
   * @readonly
   * @public
   * @static
   * @override
   */
   public static readonly state: GameStateObject = GameState.register( Playing.name );

  /**
   * gameplay type
   * @see PlayingType
   * @type {PlayingType}
   * @private
   */
  private gameplayType:PlayingType;

  /**
   * Current example reference
   * @type {Example}
   * @private
   */
   private currentExample:Example;

   /**
    * Current x offset
    * @type {number}
    * @private
    */
   private currentOffsetX:number = 0;

   /**
    * Current y offset
    * @type {number}
    * @see #draw
    * @private
    */
   private currentOffsetY:number = 0;

   /**
    * Current speed (as floating point 0-1)
    * @type {Speed}
    * @private
    */
   private speed:Speed;

   /**
    * Current editor to draw
    * @type {Editor}
    * @private
    */
  private editor:Editor;

  /**
   * Current editor state (playing, game over bcs of player or game)
   * @type {EditorState}
   * @private
   */
  private editorState:EditorState;

  /**
   * object providing information about the size of an element and its position relative to the viewport
   * @type {DOMRect}
   * @public
   */
  public position: DOMRect = {} as DOMRect;


    /**
    * Working editor Context Panel
    * @type {ContextPanel}
    * @private
    */
   private readonly panel:ContextPanel;

  /**
   * @returns {EditorState} current editor state
   * @public
   */
  public getEditorState():EditorState {
    return this.editorState;
  }

  /**
   * Constructs new playing stage
   * @public
   * @constructor
   */
  public constructor() {
    this.panel = new ContextPanel();
    this.registerContextPanel();
    this.speed = new Speed( Difficulty.EASY, UI.customData().speedMultiplier );
  }

  /**
   * Registers context panel (adds all of the drawables to it)
   * @private
   */
  private registerContextPanel() {
    this.panel.clear();
    // Background:
    const bgObj = new Rectangle( TZNClicker.instance().getWidth(), TZNClicker.instance().getHeight() );
    bgObj.mergeEffects( { fillStyle: UI.currentTheme().background } );
    if( this.gameplayType !== PlayingType.ZEN ) // User is allowed to click keys only when playing non-zen
      bgObj.addEventListener( "keydown", this.keydown );
    this.panel.push( bgObj );
  }

  /**
   * Creates new playing area within this playing stage from given values
   * @param {Example} ex example to be loaded
   * @param {PlayingType} gameplayType gameplay type to load (offsetY behaviour)
   * @param {boolean} skipEditingLine Should skip drawing editing line that splits line numbers and editing lines
   * @see Example
   * @see PlayingType
   * @public
   */
  public load( ex:Example, gameplayType:PlayingType = PlayingType.NORMAL, skipEditingLine: boolean = false ) {
    this.currentExample = ex;
    this.editor = new Editor( this.currentExample.lines, skipEditingLine ); // TODO: Find a way to add editor to the context panel
    this.speed = new Speed( ex.difficulty, UI.customData().speedMultiplier );

    this.gameplayType = gameplayType;

    this.reset();
  }

  /**
   * Resets all of the values to their initial state
   * @public
   * @override
   */
  public reset() {
    this.registerContextPanel();
    this.speed.adjustSpeed( -this.speed.getSpeed() + this.currentExample.difficulty );
    this.currentOffsetX = 0;
    if( this.gameplayType !== PlayingType.ZEN )
      this.currentOffsetY = TZNClicker.instance().getHeight();
    else
      this.currentOffsetY = 0;
    this.editorState = EditorState.PLAYING;
    this.editor.adjustCurrentChar( -this.editor.getCurrentChar() );
  }

  /**
   * Forwards event call to the given stage if it has given type implemented
   * @param {SupportedEventTypes} event event to be forwarded
   * @public
   */
  public callEvent( event:SupportedEventTypes ) {
    this.panel.callEvent( event );
  }

  /**
   * Method invoked by the ContextPanel on bg when any key was pressed
   * @param {KeyboardEvent} event keyboard event to be handled
   * @override
   * @public
   */
  private keydown( event: KeyboardEvent ):void {
    if( event.key.length > 1 )
      switch( event.key ) {
        case "Escape":
          // TODO: Optimize this:
          // TODO: TZNClicker.instance().getDrawer().getLoader().load( Stopped.state );
        default:
          this.editorState = EditorState.PLAYING;
          return;
      }
    const keyCode = event.key.charCodeAt( 0 );
    if( keyCode >= 33 && keyCode <= 126 ) { // see ASCII table
      if( keyCode === this.currentExample.raw.charCodeAt( this.editor.getCurrentChar() ) ) {
        // TODO: Animation
        this.editor.adjustCurrentChar( 1 );
      } else {
        this.editorState = EditorState.GAME_OVER;
        return;
      }
    }
    if( this.editor.getCurrentChar() + 1 === this.currentExample.raw.length ) { // Complete end
      this.editorState = EditorState.GAME_END;
      return;
    }
  }

  /**
   * Draws this playing stage and given coordinates
   * @param {CanvasRenderingContext2D} ctx rendering context
   * @param {number} x drawing x coordinates
   * @param {number} y drawing y coordinates
   *
   * @public
   * @override
   */
  public draw(ctx: CanvasRenderingContext2D, x: number, y: number) {
    this.position.x = x;
    this.position.y = y;
    ctx.font = `${UI.currentTheme().dimens.fontSize}px ${UI.currentTheme().dimens.fontFamily1}`;

    this.panel.draw( ctx, 0, 0 );

    let oldLine = this.editor.getCurrentLine();

    this.editor.draw( ctx, x + this.currentOffsetX, y + this.currentOffsetY );

    switch( this.gameplayType ) {
      case PlayingType.AUTO:
        if( this.currentOffsetY % ( this.speed.get() * 4 ) === 0 )
          this.editor.adjustCurrentChar( 1 );
      case PlayingType.NORMAL:
        this.currentOffsetY -= this.speed.get();
        break;
      default: case PlayingType.ZEN:
        if( this.editor.getCurrentLine() > oldLine )
          this.currentOffsetY += UI.lineSize( LineOrientation.VERTICAL ); // TODO: Swing animation
    }

    if( this.editor.ended() ) { // User out of time
      switch( this.gameplayType ) {
        case PlayingType.AUTO:
          this.reset();
          this.editor.adjustCurrentChar( this.editor.getCurrentChar() );
          this.editor.ended( false );
          break;
        default:
      }
      this.editorState = EditorState.GAME_OVER;
    }
    switch( this.editorState ) {
      case EditorState.GAME_END:
        // TODO: TZNClicker.instance().getDrawer().getLoader().load( GameEnd.state );
        break;
      case EditorState.GAME_OVER:
        // TODO: TZNClicker.instance().getDrawer().getLoader().load( GameOver.state );
        break;
      default:
    }
    this.position.width = this.editor.position.width;
    this.position.height = this.editor.position.height;
  }

  /**
  * @returns {number} current x offset
  * @public
  */
  public getCurrentOffsetX():number {
    return this.currentOffsetX;
  }

  /**
   * @returns {number} current y offset
   * @public
   */
  public getCurrentOffsetY():number {
    return this.currentOffsetY;
  }

  /**
   * @returns {Example} current example
   * @public
   */
  public getCurrentExample():Example {
    return this.currentExample;
  }

  /**
   * @returns {Speed} current speed
   * @public
   */
  public getCurrentSpeed():Speed {
    return this.speed;
  }

}
