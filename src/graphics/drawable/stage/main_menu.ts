/**
 * playing.ts - playing stage implementation
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

 "use strict";

import { staticImplements } from "../../../../libraries/js-helpers/src/utils"
import { fastOfflineExamples } from "../../../assets/offline_examples";
import { Speed } from "../../../../libraries/typeore/src/assets/speed";
import { UI } from "../../../../libraries/typeore/src/assets/ui";
import { GameState, GameStateObject } from "../../../../libraries/typeore/src/game_state";
import { TZNClicker } from "../../../tznclicker";
import { ContextPanel } from "../../../../libraries/typeore/src/graphics/drawable/context_panel";
import { Rectangle } from "../../../../libraries/typeore/src/graphics/drawable/shape/rectangle";
import { Text } from "../../../../libraries/typeore/src/graphics/drawable/shape/text";
import { Playing, PlayingType } from "./playing";
import { Stage, StageState } from "../../../../libraries/typeore/src/graphics/drawable/stage/stage";
import { SupportedEventTypes } from "../../../../libraries/typeore/src/graphics/drawable/interactive/interactive";

/**
  * MainMenu stage implementation
  * @class
  * @implements Stage
  * @implements StageState
  */
 @staticImplements< StageState >()
export class MainMenu implements Stage {

  /**
  * GameState corresponding to this stage
  * @type {GameStateObject}
  * @readonly
  * @public
  * @static
  * @override
  */
  public static readonly state: GameStateObject = GameState.register( MainMenu.name );

  /**
   * Current speed (as floating point 0-1)
   * @type {Speed}
   * @private
   */
  private speed:Speed;

  /**
  * Working editor Context Panel
  * @type {ContextPanel}
  * @private
  */
  private readonly panel:ContextPanel;

  /**
   * object providing information about the size of an element and its position relative to the viewport
   * @type {DOMRect}
   * @public
   */
  public position: DOMRect = {} as DOMRect;

  /**
   * Forwards event call to the given stage if it has given type implemented
   * @param {SupportedEventTypes} event event to be forwarded
   * @public
   */
   public callEvent( event:SupportedEventTypes ) {
     this.panel.callEvent( event );
  }

  /**
  * Constructs new Main menu stage
  * @param {number} numOfBgAnimations number of playing auto animations in background of Main menu
  * @param {number} speedMultiplier speed multiplier (defaults to 1.0) greater value == greater speed
  * @public
  * @constructor
  */
  public constructor() {
    this.panel = new ContextPanel();

    this.registerContextPanel();

    this.speed = new Speed( 1, UI.customData().speedMultiplier );
  }

  /**
   * Registers context panel (adds all of the drawables to it)
   * @private
   */
  private registerContextPanel() {
    this.panel.clear();
    // Background:
    const bgObj = new Rectangle( TZNClicker.instance().getWidth(), TZNClicker.instance().getHeight() );
    this.panel.push( bgObj, { fillStyle: UI.currentTheme().background } );

    const langs:Array<string> = Object.keys( fastOfflineExamples );

    for( let i = 0; i < UI.customData().numOfBgAnimations; ++i ) {
      const lang   :string = langs[ Math.floor( Math.random() * langs.length ) ];
      const example:number = Math.floor( Math.random() * fastOfflineExamples[ lang ].length );

      const obj = new Playing();
      obj.load( fastOfflineExamples[ lang ][ example ], PlayingType.AUTO, true );

      this.panel.push(
        obj,
        {
          scale: { x: 0.6, y: 0.6 },
          rotate: { value: (Math.PI / 180) * ( Math.random() * 180 - 90 ) },
          translate: {
            x: TZNClicker.instance().getWidth() * (i/UI.customData().numOfBgAnimations),
            y: 0
          },
          globalAlpha: 0.7
        }
      );
    }

    const textObj = new Text( TZNClicker.instance().gameName );
    this.panel.push(
      textObj,
      {
        translate: {
          x: 0,
          y: TZNClicker.instance().getHeight() * 0.3
        },
        fillStyle: UI.currentTheme().foreground,
        font: `${UI.currentTheme().dimens.fontSize*2.5}px ${UI.currentTheme().dimens.fontFamily2}`,
        centerX: { text: textObj.text() }
      }
    );
  }

  /**
  * Resets all of the values to their initial state
  * @public
  * @override
  */
  public reset() {
    this.registerContextPanel();
    this.speed.adjustSpeed( -this.speed.getSpeed() + 1 );
  }

  /**
  * Draws this main menu stage and given coordinates
  * @param {CanvasRenderingContext2D} ctx rendering context
  * @param {number} x drawing x coordinates
  * @param {number} y drawing y coordinates
  *
  * @public
  * @override
  */
  public draw(ctx: CanvasRenderingContext2D, x: number, y: number) {
    this.position.x = x;
    this.position.y = y;
    ctx.font = `${UI.currentTheme().dimens.fontSize}px ${UI.currentTheme().dimens.fontFamily2}`;

    this.panel.draw( ctx, 0, 0 );

    // TODO: Main menu content
    this.position.width = this.panel.position.width;
    this.position.height = this.panel.position.height;
  }

  /**
  * @returns {Speed} current speed
  * @public
  */
  public getCurrentSpeed():Speed {
    return this.speed;
  }

 }
