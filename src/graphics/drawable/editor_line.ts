/**
 * line.ts - Single editor line implementation
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { HighlightTypes, LineType } from "../../assets/highlight";
import { LineOrientation, UI } from "../../../libraries/typeore/src/assets/ui";
import { Drawable } from "../../../libraries/typeore/src/graphics/drawable/drawable";
import { LineNumber } from "./line_number";
import { TZNClickerCustomDataType } from "../../assets/config";

/**
 * Single Editor Line representation
 * @class
 * @extends LineNumber
 * @implements Drawable
 */
export class EditorLine extends LineNumber implements Drawable {

  /**
   * Current line data (with all the operations and their content)
   * @type {!LineType}
   * @private
   */
  private line!:LineType;

  /**
   * Number of all the characters (excluding whitespaces - START, END, NULL, TAB, NEW_LINE, SPACE etc.)
   * @type {number}
   * @private
   */
  private charsNum:number = 0;

  /**
   * Number of characters without transparency to be drawn in this line
   * @type {number}
   * @private
   */
  private noTransparencyChars:number;

  /**
   * object providing information about the size of an element and its position relative to the viewport
   * @type {DOMRect}
   * @public
   */
  public position: DOMRect = {} as DOMRect;

  /**
   * Constructs new editor line from the LineType
   * @param {LineType} line line from the object will be constructed
   *
   * @see LineType
   * @override
   * @public
   * @constructor
   */
  public constructor( line:LineType, index:number ) {
    super( index );
    this.line = line;
    // Count all of the characters (excluding whitespaces):
    for( const op of this.line )
      switch( op.type ) {
        case HighlightTypes.START: case HighlightTypes.SPACE: case HighlightTypes.NEW_LINE:
        case HighlightTypes.END:   case HighlightTypes.NULL:  case HighlightTypes.TAB:
          break;
        default:
          this.charsNum += op.content.length;
      }
  }

  /**
   * Draws this editor line using given canvas rendering context at given coordinates
   *
   * @param {CanvasRenderingContext2D} ctx canvas context for the line to be drawn
   * @param {number} x x coordinates for the line to be drawn
   * @param {number} y y coordinates for the line to be drawn TODO: (auto increments the value on NEW_LINE op type)
   * @param {number} numOfCharsWithoutTransparency how many characters of this line should be drawn without transparency (0 or negative value for none)
   *
   * @override
   * @public
   * @static
   */
  public draw( ctx:CanvasRenderingContext2D, x:number, y:number ) {
    this.position.x = x;
    this.position.y = y;
    super.draw( ctx, x + UI.currentTheme().dimens.leftMarginSize, y );

    const savedGlobalAlpha = ctx.globalAlpha;

    let changedNumOfCharsWithoutTransparency = this.noTransparencyChars;

    x += UI.currentTheme().dimens.rightMarginSize
      +  UI.currentTheme().dimens.leftMarginSize
      +  UI.currentTheme().dimens.strokeSize; // Gap after drawing lineNumber

    let maxX = x;

    const defFontWidthSize = UI.lineSize( LineOrientation.HORIZONTAL, false );
    const defFontHeightSize = UI.lineSize( LineOrientation.VERTICAL );

    for( const op of this.line ) {
      switch( op.type ) { // Skip counting white chars to charsApplied, x, y );
        case HighlightTypes.TAB:
          x += ( UI.currentTheme().dimens.tabIndentation - 1 ) * defFontWidthSize; // Tab is 2 spaces indent
          y -= op.content.length * defFontHeightSize; // Ignore new line
        case HighlightTypes.NEW_LINE:
          y += op.content.length * defFontHeightSize;
        case HighlightTypes.START: case HighlightTypes.SPACE:
        case HighlightTypes.END: case HighlightTypes.NULL:
          ctx.fillStyle = UI.customData<TZNClickerCustomDataType>().playground[ op.type ];
          ctx.fillText( op.content, x, y );
          x += op.content.length * defFontWidthSize;
          break;
        default:
          // TODO: Optimization
        for( const char of op.content ) {
          let color = UI.customData<TZNClickerCustomDataType>().playground[ op.type ];
          if( changedNumOfCharsWithoutTransparency <= 0 ) {
            ctx.globalAlpha *= 0.5;
          } else {
            --changedNumOfCharsWithoutTransparency;
          }
          ctx.fillStyle = color;
          ctx.fillText( char, x, y );
          ctx.globalAlpha = savedGlobalAlpha;
          x += defFontWidthSize;
        }
      }
      maxX = Math.max( maxX, x );
    }
    super.isCurrent( this.noTransparencyChars > 0 && changedNumOfCharsWithoutTransparency <= 0 );
    this.position.width = maxX - this.position.x;
    this.position.height = y;
  }

  /**
   * Sets or retrieves the current number of characters without transparency
   * @param {number|null} value value for the no transparency chars to be set or null if you just want to retrieve the value (defaults to null)
   * @returns {number} current numer of characters without transparency
   * @public
   */
  public numOfCharsWithoutTransparency( value:number|null ):number {
    return ( value !== null ? this.noTransparencyChars = value : this.noTransparencyChars );
  }

  /**
   * Number of all the characters in this line (excluding whitespaces - START, END, NULL, TAB, NEW_LINE, SPACE etc.)
   * @returns {number} number of all the characters in this line (excluding whitespaces)
   * @public
   */
  public getSumOfChars():number {
    return this.charsNum;
  }

}
