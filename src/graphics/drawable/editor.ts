/**
 * editor.ts - editor_line container
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { staticAssert } from "../../../libraries/js-helpers/src/assert";
import { LineType } from "../../assets/highlight";
import { Stroke } from "../../../libraries/typeore/src/graphics/drawable/shape/stroke";
import { UI, LineOrientation } from "../../../libraries/typeore/src/assets/ui";
import { Drawable } from "../../../libraries/typeore/src/graphics/drawable/drawable";
import { EditorLine } from "./editor_line";

/**
 * In game editor state
 * @enum
 */
export enum EditorState {
  /**
   * Playing
   */
  PLAYING,
  /**
   * Game over
   */
  GAME_OVER,
  /**
   * User has successfully ended the game
   */
  GAME_END
}

/**
 * Editor holding editor_lines
 * @class
 * @implements Drawable
 */
 export class Editor implements Drawable {

  // TODO: Add context panel here

  /**
   * Array of lines ready to draw
   * @type {Array<EditorLine>}
   * @private
   * @default []
   */
   private lines:Array<EditorLine> = [];

   /**
    * current char index in editor
    * @type {number}
    * @private
    */
   private currentChar:number = 0;

   /**
    * line number stroke separating editor line and line number
    * @type {Stroke}
    * @private
    */
  private lineNumbersStroke:Stroke;

  /**
   * Holds current line as an index
   * @type {number}
   * @private
   */
  private currentLine:number = 0;

  /**
   * Indicates if the game has ended
   * @type {boolean}
   * @private
   */
  private hasEnded:boolean = false;

  /**
   * object providing information about the size of an element and its position relative to the viewport
   * @type {DOMRect}
   * @public
   */
  public position: DOMRect = {} as DOMRect;

   /**
    * Constructs a new Editor object containing Editor lines and ready to be drawn
    * @param {Array<LineType>} lines array of linetype arrays that can be retrieved for example from fastOfflineExamples
    * @param {boolean} skipEditingLine Should skip drawing editing line that splits line numbers and editing lines
    *
    * @see fastOfflineExamples
    * @see LineType
    *
    * @public
    * @constructor
    */
  public constructor( lines: Array<LineType>, skipEditingLine: boolean = false ) {
    this.lineNumbersStroke = new Stroke( UI.lineSize( LineOrientation.HORIZONTAL ), 0 );
    this.lineNumbersStroke.mergeEffects( {
      lineWidth: UI.currentTheme().dimens.strokeSize,
      strokeStyle: UI.currentTheme().secondary
    } );
    if( skipEditingLine )
      this.lineNumbersStroke.mergeEffects( { strokeStyle: "transparent" } );

    for( let i = 0; i < lines.length; ++i )
      this.lines.push( new EditorLine( lines[ i ], i + 1 ) ); // Populate the editor lines
  }

  /**
   * Retrieves current char
   * @returns {number} current char index
   * @public
   */
  public getCurrentChar(): number {
    return this.currentChar;
  }

  /**
   * Sets or retrieves the ended property
   * @param {boolean|null} ended value to be set or null to just retrieve the object (defaults to null)
   * @returns {boolean} either true or false if the game has ended (current line is no longer visible)
   */
  public ended( ended:boolean|null = null ):boolean {
    return ( ended !== null ? this.hasEnded = ended : this.hasEnded );
  }

  /**
   * Adjusts current char index by the given value
   * @param {number} by value for the index to be adjusted
   * @public
   *
   * @throws {AssertionError} if trying to decrease current char index to a negative value
   */
  public adjustCurrentChar( by:number ) {
    staticAssert( this.currentChar + by >= 0, "Current char index cannot be decreased to a negative value", { currentChar: this.currentChar, by } );
    this.currentChar += by;
  }

  /**
   * @returns {number} current line index
   * @public
   */
  public getCurrentLine():number {
    return this.currentLine;
  }

  /**
   * Draws all of the editor lines
   * @param {CanvasRenderingContext2D} ctx rendering context
   * @param {number} x drawing x coordinates
   * @param {number} y drawing y coordinates
   *
   * @public
   * @override
   */
   public draw( ctx: CanvasRenderingContext2D, x: number, y: number ) {
     this.position.x = x;
     this.position.y = y;
    let sumOfCharsApplied:number = 0;

    const lineSizeHorizontal = UI.lineSize( LineOrientation.HORIZONTAL );
    const lineSizeVertical = UI.lineSize( LineOrientation.VERTICAL );

    // Ddraw separating line
    this.lineNumbersStroke.xTo( x + lineSizeHorizontal );
    this.lineNumbersStroke.draw( ctx, x + lineSizeHorizontal, ctx.canvas.clientHeight );

    let linesVisible = 0, maxWidth = 0;

    for( const line of this.lines ) {
      y += lineSizeVertical;

      if( y - lineSizeVertical < ctx.canvas.clientHeight )
      {
        if( y > 0 - lineSizeVertical ) {
          // Draw only lines that are visible to save computers computing power
          line.numOfCharsWithoutTransparency( this.currentChar - sumOfCharsApplied );
          line.draw( ctx, x, y );
          if( line.isCurrent() )
            this.currentLine = line.getIndex();
          maxWidth = Math.max( maxWidth, line.position.width );
          ++linesVisible;
        } else if( line.getIndex() == this.currentLine ) {
          this.hasEnded = true;
        }
      }

      sumOfCharsApplied += line.getSumOfChars();
    }

  this.position.width = maxWidth;
  this.position.height = linesVisible * lineSizeVertical;

  }
}
