/**
 * line_number.ts - line number drawable interface
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { LineOrientation, UI } from "../../../libraries/typeore/src/assets/ui";
import { Drawable } from "../../../libraries/typeore/src/graphics/drawable/drawable";

 /**
  * line number drawable container
  * @class
  * @implements Drawable
  */
export class LineNumber implements Drawable {

  /**
   * Indicates if this line should be highlighted
   * @type {boolean}
   * @private
   */
   private currentLine:boolean;

   /**
    * This line number
    * @type {number}
    * @private
    */
   private index:number;

   /**
    * object providing information about the size of an element and its position relative to the viewport
    * @type {DOMRect}
    * @public
    */
   public position: DOMRect = {} as DOMRect;

   /**
    * Constructs new LineNumber drawable object
    * @param {number} index this line index (number)
    * @param {boolean} currentLine should be current line (defaults to false)
    *
    * @public
    * @constructor
    */
   public constructor( index:number, currentLine:boolean = false ) {
     this.index = index;
     this.currentLine = currentLine;
   }

   /**
    * Draws this line number auto detecting if it should be highlighted or not
    * @param {CanvasRenderingContext2D} ctx canvas context for the line number to be drawn
    * @param {number} x x coordinates
    * @param {number} y y coordinates
    * @public
    */
  public draw(ctx: CanvasRenderingContext2D, x: number, y: number) {
    this.position.x = x;
    this.position.y = y;
    if( this.isCurrent() )
      ctx.fillStyle = UI.currentTheme().foreground;
    else
      ctx.fillStyle = UI.currentTheme().secondary;
    ctx.fillText( this.index + "", x, y );
    this.position.width = `${this.index}`.length * UI.lineSize( LineOrientation.HORIZONTAL, false );
    this.position.height = UI.lineSize( LineOrientation.VERTICAL, false );
  }

  /**
   * Sets or retrives if this line is current line
   * @param {boolean|null} value value to be set or null if you want to retrieve current isCurrent value
   * @returns {boolean} either true or false if this line is a current line
   * @public
   */
  public isCurrent( value:boolean|null = null ):boolean {
    return ( value !== null ? this.currentLine = value : this.currentLine );
  }

  /**
   * @returns {number} this line index
   * @public
   */
  public getIndex():number {
    return this.index;
  }

}
