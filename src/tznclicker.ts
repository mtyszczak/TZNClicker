/**
 * game.ts - Main TZNClicker interface
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

import { staticAssert } from "../libraries/js-helpers/src/assert";
import { UI } from "../libraries/typeore/src/assets/ui";
import { GameCore } from "../libraries/typeore/src/core";
import { Game } from "../libraries/typeore/src/game";
import { GameError } from "../libraries/typeore/src/game_error";
import { CtxDrawer } from "../libraries/typeore/src/graphics/ctx_drawer";
import { TZNClickerCustomData, TZNClickerTheme } from "./assets/config";
import { MainMenu } from "./graphics/drawable/stage/main_menu";
import { Playing } from "./graphics/drawable/stage/playing";

/**
 * Main TZNClicker class
 * @class
 * @extends GameCore
 */
 export class TZNClicker extends GameCore implements Game {

  /**
   * This game name
   * @type {string}
   * @public
   */
  public gameName:string = "TZNClicker";

  /**
   * Game container
   * @type {!HTMLCanvasElement}
   * @private
   */
  private container!:HTMLCanvasElement; // TODO: Instead of this.container use CtxDrawer.getContext().canvas

  /**
   * Drawing context from canvas 2d graphics
   * @type {!CtxDrawer}
   * @private
   */
  private drawer!:CtxDrawer;

  /**
   * basic drawer function used in the {@link #start} and {@link #resume} methods
   * @type {()=>void}
   * @private
   * @static
   */
  private drawerFn:()=>void = (): void => {
    try {
      this.drawer.update();
    } catch(e) {
      this.catch(e);
    }
  };

  /**
   * Handles any error thrown by the drawer
   * Note: This method handles ONLY errors thrown in async drawer (user has to handle rest of the sync exceptions)
   * @param {Error} e catched error
   */
  private catch(e:Error) {
    this.onErrorThrown( e );
    this.stop();
    // TODO: Display "restart button" and send crash report
  }

  /**
   * that object (singletone)
   * @type {TZNClicker|null}
   * @private
   * @static
   */
  private static that:TZNClicker|null = null;

  /**
   * @returns {TZNClicker} this object
   * @throws {AssertionError} if that object is null (TZNClicker singletone not yet constructed)
   * @public
   * @static
   */
  public static instance():TZNClicker {
    staticAssert( this.that !== null, "Cannot return TZNClicker object before its construction. Use the construct method.", { gameObj: JSON.stringify( this.that ) } );
    return this.that;
  }

  /**
   * Constructs new global this game object and registers default stages
   * @param {HTMLCanvasElement} container game container object
   * @param {number} width width of the container or -1 to be set automatically (defaults to -1)
   * @param {number} height height of the container or -1 to be set automatically (defaults to -1)
   *
   * @returns {TZNClicker} this object
   *
   * @throws {GameError} if container is not a canvas element
   * @throws {AssertionError} if trying to reconstruct already constructed game object
   *
   * @public
   * @static
   */
  public static construct( container:HTMLCanvasElement, width:number = -1, height:number = -1 ):TZNClicker {
    staticAssert( this.that === null, "Cannot reconstruct already constructed game object", { gameObj: JSON.stringify( this.that ) } );
    this.that = new TZNClicker( container, width, height );
    this.that.registerDefaultStages();
    return this.instance();
  }

  /**
   * Constructs new TZNClicker game object. Calls constructor of the super class ({@link GameCore})
   * Resizes container to the given values if specified
   * This constructor is private due to the singletone concept.
   * In order to create a new object use {@link #construct}
   * @param {HTMLCanvasElement} container game container object
   * @param {number} width width of the container or -1 to be set automatically (defaults to -1)
   * @param {number} height height of the container or -1 to be set automatically (defaults to -1)
   *
   * @throws {GameError} if container is not a canvas element
   *
   * @private
   * @constructor
   * @see GameCore
   * @see #construct
   */
  private constructor( container:HTMLCanvasElement, width:number = -1, height:number = -1 ) {
    super(); // GameCore
    this.container = container;

    // Resize the container if needed
    this.resize( width, height );

    // Create drawing context
    if( typeof container.getContext !== "function" )
      throw new GameError( "Container has to be a canvas element" );
    this.drawer = new CtxDrawer( container.getContext("2d") );

    this.registerDefaultEvents(); // onkeydown, onpaste

    // load game data and theme
    UI.customData( TZNClickerCustomData );
    UI.currentTheme( TZNClickerTheme );
  }

  /**
   * Registers default stages to the game
   * Currently those are: MainMenu, Playing
   * @see Stage
   * @private
   */
   private registerDefaultStages() {
    this.drawer.getLoader().register( new MainMenu() );
    this.drawer.getLoader().register( new Playing() );
//    this.loader.register( new Stopped()  );
//    this.loader.register( new GameOver() );
    this.drawer.getLoader().load( MainMenu.state );
  }

  /**
   * Registers default events like on key down and on paste
   * @private
   */
  private registerDefaultEvents() {
    this.container.onpaste =
      /**
       * Helpful event handler that prevents user from cheating :)
       * @param {ClipboardEvent} event caught clipboard event
       * @throws {GameError} if user tried to cheat
       */
      (event:ClipboardEvent) => {
        event.preventDefault();
        this.catch( new GameError( "Do not try to cheat ;)" ) );
      };
  }

  public resize( width:number = -1, height:number = -1 ) {
    // Auto set width and height if width or height is less than 0
    if( width < 0 )
      width = this.container.getBoundingClientRect().width;
    if( height < 0 )
      height = this.container.getBoundingClientRect().height;

    this.onContainerResize( { x: width, y: height } );

    if( TZNClicker.that !== null ) { // Invokes ctx scale only after initialization
      const effects = this.drawer.getLoader().getEffects();
      effects.scale = {
        x: effects.scale.x * ( width / this.getWidth() ),
        y: effects.scale.y * ( height / this.getHeight() )
      }
    }

    this.container.style.width  = `${width}px`;
    this.container.style.height = `${height}px`;
  }

  /**
   * @returns {HTMLCanvasElement} container
   * @public
   */
  public getContainer():HTMLCanvasElement {
    return this.container;
  }

  /**
   * @returns {number} width of the container
   * @public
   */
  public getWidth():number {
    return this.container.clientWidth;
  }

  /**
   * @returns {number} height of the container
   * @public
   */
  public getHeight():number {
    return this.container.clientHeight;
  }

  /**
   * @returns {CtxDrawer} drawer assigned to this class
   * @public
   */
  public getDrawer():CtxDrawer {
    return this.drawer;
  }

  /**
   * Starts the game and resets the drawer
   * @throws {GameError} on initialization error
   * @see CtxDrawer#reset
   * @public
   * @override
   */
   public start() {
    this.onStart();
    if( super.isRunning() )
      throw new GameError( "Trying to restart the game without calling reset before" );

    super.start();
    this.drawer.reset();
    this.drawer.start( this.drawerFn );
  }

  /**
   * Resumes the game without resetting the values
   * @throws {GameError} on initialization error
   * @public
   */
  public resume() {
    this.onResume();
    if( super.isRunning() )
      throw new GameError( "Trying to resume the game without calling stop before" );

    super.start();
    this.drawer.start( this.drawerFn );
  }

  /**
   * Stops the game
   * @public
   * @override
   */
  public stop() {
    this.onStop();
    super.stop();
    this.drawer.stop();
  }

  /**
   * Resets the game
   * @public
   */
  public reset() {
    this.onReset();
    if( super.isRunning() )
      this.stop();
    this.drawer.reset();
    this.start();
  }
}
