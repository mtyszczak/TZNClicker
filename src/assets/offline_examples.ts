/**
 * offline_examples.ts - some basic fast offline examples
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { HighlightTypes, Difficulty, Languages } from "./highlight";

/**
 * Fast offline examples (just for testing purposes)
 // XXX: Remove in production
 * @constant
 */
export const fastOfflineExamples:Languages = {
  "C++": [
    {
      name: "Hello, world!",
      raw: "#include<iostream>intmain(){std::cout<<\"Hello,world!\"<<std::endl;return0;}",
      difficulty: Difficulty.EASY,
      lines: [
        [
          { type: HighlightTypes.START,        content: ""           },
          { type: HighlightTypes.PREPROCESSOR, content: "#include"   },
          { type: HighlightTypes.SPACE,        content: " " },
          { type: HighlightTypes.STRING,       content: "<iostream>" },
          { type: HighlightTypes.NEW_LINE,     content: "\n"         }
        ],
        [
          { type: HighlightTypes.NEW_LINE,     content: "\n" }
        ],
        [
          { type: HighlightTypes.KEYWORD,      content: "int" },
          { type: HighlightTypes.SPACE,        content: " " },
          { type: HighlightTypes.IDENTIFIER,   content: "main" },
          { type: HighlightTypes.CHARACTERS,   content: "()" },
          { type: HighlightTypes.NEW_LINE,     content: "\n" }
        ],
        [
          { type: HighlightTypes.CHARACTERS,   content: "{" },
          { type: HighlightTypes.NEW_LINE,     content: "\n" }
        ],
        [
          { type: HighlightTypes.TAB,          content: "\t" },
          { type: HighlightTypes.IDENTIFIER,   content: "std" },
          { type: HighlightTypes.CHARACTERS,   content: "::" },
          { type: HighlightTypes.IDENTIFIER,   content: "cout" },
          { type: HighlightTypes.SPACE,        content: " " },
          { type: HighlightTypes.CHARACTERS,   content: "<<" },
          { type: HighlightTypes.SPACE,        content: " " },
          { type: HighlightTypes.STRING,       content: "\"Hello, world!\"" },
          { type: HighlightTypes.SPACE,        content: " " },
          { type: HighlightTypes.CHARACTERS,   content: "<<" },
          { type: HighlightTypes.SPACE,        content: " " },
          { type: HighlightTypes.IDENTIFIER,   content: "std" },
          { type: HighlightTypes.CHARACTERS,   content: "::" },
          { type: HighlightTypes.IDENTIFIER,   content: "endl" },
          { type: HighlightTypes.CHARACTERS,   content: ";" },
          { type: HighlightTypes.NEW_LINE,     content: "\n" }
        ],
        [
          { type: HighlightTypes.TAB,          content: "\t" },
          { type: HighlightTypes.KEYWORD,      content: "return" },
          { type: HighlightTypes.SPACE,        content: " " },
          { type: HighlightTypes.NUMBER,       content: "0" },
          { type: HighlightTypes.CHARACTERS,   content: ";" },
          { type: HighlightTypes.NEW_LINE,     content: "\n" }
        ],
        [
          { type: HighlightTypes.CHARACTERS,   content: "}" },
          { type: HighlightTypes.END,          content: "" }
        ]
      ]
    }
  ]
};
