/**
 * theme.ts - tznclicker theme data and custom assets
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { Theme } from "../../libraries/typeore/src/assets/ui";
import { HighlightTypes } from "./highlight";

/**
 * Playground type must be HighlightType: string
 * @see HighlightTypes
 */
export type Playground = {
  [ key in /* keyof typeof */ HighlightTypes ]: string;
};

/**
 * TZNClicker theme to be used in app
 * @type {Theme}
 * @constant
 */
export const TZNClickerTheme:Theme = {
  background: "#282c34",
  secondary: "#5a5a5a",
  foreground: "#dadada",

  dimens: {
    strokeSize: 6,
    fontSize: 14,
    fontFamily1: "monospace",
    fontFamily2: "Arial",
    leftMarginSize: 12,
    rightMarginSize: 12,
    topMarginSize: 3,
    bottomMarginSize: 3,
    tabIndentation: 2
  }
};

/**
 * TZNClicker custom data type specification
 * @interface
 */
export interface TZNClickerCustomDataType {
  playground:Playground;
  numOfBgAnimations:number;
  speedMultiplier:number;
}

/**
 * Custom tznclicker data type
 */
export const TZNClickerCustomData:TZNClickerCustomDataType = { // TODO: Color class for the transparency support
  playground: {
    /* START:         */ 0:  "transparent",
    /* END:           */ 1:  "transparent",
    /* SPACE:         */ 2:  "transparent",
    /* TAB:           */ 3:  "transparent",
    /* NEW_LINE:      */ 4:  "transparent",
    /* PREPROCESSOR:  */ 5:  "#ff6363",
    /* KEYWORD:       */ 6:  "#ff6363",
    /* IDENTIFIER:    */ 7:  "#c054ff",
    /* VARIABLE:      */ 8:  "#dadada",
    /* COMMENT:       */ 9:  "#4a4a4a",
    /* DOCUMENTATION: */ 10: "#4a4a4a",
    /* CHARACTERS:    */ 11: "#dadada",
    /* NUMBER:        */ 12: "#38a3d1",
    /* STRING:        */ 13: "#38a3d1",
    /* NULL:          */ 14: "transparent"
  },
  numOfBgAnimations: 3,
  speedMultiplier: 1.0
};
