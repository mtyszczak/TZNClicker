/**
 * highlight.ts - highlight interface
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

/**
 * Highlight types
 * @enum
 */
export enum HighlightTypes {
  START /* TODO: maybe = "START" ... */,
  END,
  SPACE,
  TAB,
  NEW_LINE,

  PREPROCESSOR,
  KEYWORD,

  IDENTIFIER,
  VARIABLE,

  COMMENT,
  DOCUMENTATION,

  CHARACTERS,

  NUMBER,
  STRING,

  NULL /* Does nothing */
};

/**
 * Returns highlight type name from the given index in enum
 * @param {HighlightTypes|number} index enum index
 * @returns {string} key name in enum corresponding to the given index
 * @constant
 */
export const highlightTypeNameRetriever = (index:HighlightTypes|number):string => {
  const keys = Object.keys( HighlightTypes );
  return keys[keys.length/2+index];
};

/**
 * Difficulty enum
 * @enum
 */
export enum Difficulty {
  EASY = 1,
  MEDIUM = 2,
  HARD = 3
}

/**
 * Line operation (type and string content to be displayed)
 * @interface
 */
export interface LineOperation {
  type: HighlightTypes;
  content:string;
}

/**
 * Basic line type that is an array of line operations
 * @see LineOperation
 */
export type LineType = Array<LineOperation>;

/**
 * Single example
 *
 * Contains example name, raw representation of the example (without whitespaces),
 * difficulty and lines
 * @see Difficulty
 * @see LineType
 * @interface
 */
export interface Example {
  name:string;
  raw:string;
  difficulty:Difficulty;
  lines:Array<LineType>;
}

/**
 * Languages interface cotnaining all of the supported languages
 *
 * @see Example
 * @interface
 */
export interface Languages {
  "C++":Array<Example>;
}
