# TZNClicker

[![pipeline status](https://gitlab.com/mtyszczak/TZNClicker/badges/master/pipeline.svg)](https://gitlab.com/mtyszczak/TZNClicker/-/commits/master)
[![coverage report](https://gitlab.com/mtyszczak/TZNClicker/badges/master/coverage.svg)](https://gitlab.com/mtyszczak/TZNClicker/-/commits/master)
[![License GNU GPL v3](https://img.shields.io/badge/License-GNU%20GPL%20v3-red.svg)](LICENSE)
[![Version 0.1](https://img.shields.io/badge/Version-0.1-abc.svg)](https://gitlab.com/mtyszczak/TZNClicker/-/commits/master)

<p align="center"><img height="128" width="128" alt="TZNClicker Logo" src="https://gitlab.com/mtyszczak/TZNClicker/-/raw/develop/examples/game_loading/assets/images/tznclicker-logo.png"></p>

A simple clicker encouraging you to choose the Programming learning profile

## Running
This game is supposed to work on every device that has a built-in web browser that supports HTML5, CSS3 and ES2017.
Simply open the file named `index.html` located in the `examples` directory

## CDN
You can download compiled sources from the `examples/*/assets/js` directory or link them in your project:
```js
<script src="https://gitlab.com/mtyszczak/TZNClicker/-/raw/master/examples/game_loading/assets/js/tznclicker-0.1.js"></script>
```

## Compiling
Initialize libraries: `git submodule update --init --recursive --progress`
Run `build.sh` script located in the root directory of this project to compile the game into the javascript. Use `--help` option to display builder help.

## Editing

### Website files layout
All of the page assets can be found in the `examples/*/assets` directory.

### JavaScript files copying
Some of the typescript files are **dependent** from each other (if not so it will be mentioned in the documentation or Wiki)

## Documentation
You can use [Repo Wiki](https://gitlab.com/mtyszczak/TZNClicker/-/wikis/home) or generated documentation available in the `wiki` directory
To regenerate documentation run `build.sh -d`

## License
Distributed under the GNU GENERAL PUBLIC LICENSE Version 3
See [LICENSE](LICENSE)