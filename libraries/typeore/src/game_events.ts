/**
 * game_events.ts - All of the game events
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { Coordinates } from "./graphics/drawable/animation/animation";

/**
 * Game event handlers class
 * @class
 */
export class GameEvents {

  // Event handlers:

  /**
   * triggers every time start() method is called
    * @type {()=>void}
   * @see Game#start
   * @public
   * @event
   */
   public onStart:()=>void  = (): void => {};

   /**
    * triggers every time stop() method is called
    * @type {()=>void}
    * @see Game#stop
    * @public
    * @event
    */
   public onStop:()=>void   = (): void => {};

   /**
    * triggers every time resume() method is called
    * @type {()=>void}
    * @see Game#resume
    * @public
    * @event
    */
   public onResume:()=>void = (): void => {};

   /**
    * triggers every time reset() method is called
    * @type {()=>void}
    * @see Game#reset
    * @public
    * @event
    */
   public onReset:()=>void  = (): void => {};

   /**
    * triggers every time user presses a key
    * @type {(event:KeyboardEvent)=>void}
    * @public
    * @event
    */
   public onKeyPress:(event:KeyboardEvent)=>void = (_event:KeyboardEvent): void => {};

   /**
    * triggers every time any error is thrown
    * @type {(e:Error)=>void}
    * @public
    * @event
    */
   public onErrorThrown:(e:Error)=>void = (_e:Error): void => {};

   /**
    * triggers every time container has been resized (proper method has been called)
    * @type {(newSize:Coordinates)=>void}
    * @public
    * @event
    */
   public onContainerResize:(newSize: Coordinates)=>void = (_newSize:Coordinates): void => {};

}
