/**
 * game_state.ts - Game states enum
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { staticAssert } from "../../js-helpers/src/assert";

/**
 * Single game state object that contains name of the state and its id
 */
export type GameStateObject = {
  name: string;
  id: number;
}

/**
 * Game state for stages registration
 * @class
 */
export class GameState {

  /**
   * array of registered objects
   * @type {Array<GameStateObject>}
   * @private
   * @static
   */
  private static objects:Array<GameStateObject> = [] as Array<GameStateObject>;

  /**
   * Current object id
   * @type {number}
   * @private
   * @static
   */
  private static oid:number = 0;

  /**
   * Constructs new GameState object
   * @private
   * @constructor
   */
  private constructor() {}

  /**
   * Registers new game state object with given name and auto generated object id
   * Note: Accepts multiple states with the same name
   * @param {string} name name of the game state
   * @returns {GameStateObject} registered game state object
   *
   * @public
   * @static
   */
  public static register( name:string ):GameStateObject {
    return this.objects[ this.objects.push( { name, id: this.oid++ } ) - 1 ];
  }


  /**
   * Unregisters state(s) with given name
   * @param {string} name name of the game state to unregister
   * @param {boolean} removeAllOccurrences should remove all states with given name (defaults to true)
   *
   * @public
   * @static
   */
  public static unregister( name:string, removeAllOccurrences:boolean = true ) {
    let obj_length = this.objects.length;
    for( let i = 0; i < obj_length; ++i )
      if( this.objects[ i ].name === name ) {
        this.objects.splice( i, 1 );
        --obj_length;
        --i;
        if( !removeAllOccurrences )
          break;
      }
  }

  /**
   * Retrives all states with given name and limits output to the given value
   * @param {string} name name of the state
   * @param {boolean} limit limit for the output (negative value for no limit)
   * @returns {Array<GameStateObject>} All occurrences of the state object with given name
   *
   * @public
   * @static
   */
  public static get( name:string, limit:number = -1 ):Array<GameStateObject> {
    let objs:Array<GameStateObject> = [];
    for( const obj of this.objects )
      if( limit >= 0 && objs.length > limit )
        break;
      else if( obj.name === name )
        objs.push( obj );
    return objs;
  }

  /**
   * Retrives first occurrence of the state object with given name
   * @param {string} name name of the state
   * @returns {GameStateObject} first occurrence of the state object with given name
   *
   * @throws {AssertionError} if no occurrence found
   *
   * @public
   * @static
   */
  public static getOne( name:string ):GameStateObject {
    let obj:Array<GameStateObject> = this.get( name, 1 );
    staticAssert( obj.length >= 1, "Could not get game state object with name: ${name}. It might be not be registered yet", { name } );
    return obj[ 0 ];
  }

}
