/**
 * game.ts - Main game interface
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

// TODO: Namespaces

/**
 * Main game interface that every game should implement
 * If you derive from the GameCore in your game class remember also to call the super methods
 * @interface
 */
export interface Game {
  /**
   * This game name
   * @type {string}
   */
  gameName:string;

  /**
   * Starts the game.
   * Proper behaviour after calling this method in case of you wanting to stop the game is to call
   * reset or stop method
   * Note: If you derive from the GameCore remember to call the super method
   */
  start():void;

  /**
   * Stops the game.
   * In order to resume the game you should call resume method or reset the game
   * Note: If you derive from the GameCore remember to call the super method
   */
  stop():void;

  /**
   * Resumes the game.
   * Should be a valid call only after calling stop
   */
  resume():void;

  /**
   * Resets the game.
   * Should be valid in any aspect of the game (either playing or stopped)
   */
  reset():void;
}
