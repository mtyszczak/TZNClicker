/**
 * core.ts - Game core
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { Version } from '../../js-helpers/src/version';
import { GameEvents } from './game_events';

/**
 * Game core class
 * @class
 * @extends GameEvents
 */
export class GameCore extends GameEvents {

  /**
   * Keeps information about validity of the game
   * @type {!boolean}
   * @private
   */
  private running!:boolean;

  /**
   * Current game version
   * @type {!Version}
   * @private
   */
  private version!:Version;

  /**
   * Constructs new GameCore object (sets validity of the game automatically to false)
   * @public
   * @constructor
   */
  public constructor() {
    super();
    this.running = false;
    this.version = new Version( 0, 1 );
  }

  /**
   * @returns {boolean} true if game is running (already loaded)
   * @public
   */
  public isRunning():boolean {
    return this.running;
  }

  /**
   * Converts current game core version into the string and returns it
   * @returns {string} stringified game core version
   * @public
   */
  public getVersion():string {
    return this.version.stringify();
  }

  /**
   * Start the game (validates the game)
   * @protected
   */
  protected start() {
    this.running = true;
  }

  /**
   * Stop the game (invalidates the game)
   * @protected
   */
  protected stop() {
    this.running = false;
  }

}
