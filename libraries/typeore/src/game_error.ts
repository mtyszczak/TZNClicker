/**
 * game_error.ts - Game Error interface
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { AssertionError } from "../../js-helpers/src/assert";

 /**
  * Game Error interface (AssertionError forwarding class)
  * @class
  * @constructor
  * @extends AssertionError
  */
export class GameError extends AssertionError {
  /**
   * @param {string} message message for the error to be thrown
   * @param {string} description optional description returned from \b describe
   * @param {object} resolvers object with members that will be replaced in message, e.g. message: "${a1}, World!" and resolvers: {a1:"Hello"}. Output: "Hello, World!"
   */
     constructor( message: string, description:string = "", resolvers:object = {} ) {
       super( message, description, resolvers );
  }
}
