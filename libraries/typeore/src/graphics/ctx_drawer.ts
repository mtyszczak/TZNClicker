/**
 * ctx_drawer.ts - Main drawing interface
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { GameError } from "../game_error";
import { StageLoader } from "./drawable/stage_loader";

/**
 * Canvas 2d context drawer
 * @class
 */
export class CtxDrawer {

  /**
   * Canvas rendering context
   * @type {!CanvasRenderingContext2D}
   * @private
   */
  private ctx!:CanvasRenderingContext2D;

  // Drawing settings:

  /**
   * In-game pre-defined in constructor frames per second
   * @type {number}
   * @private
   */
  private fps:number;
  /**
   * game draw update interval
   * generated from fps
   * @type {number}
   * @private
   */
  private interval:number;
  /**
   * Game step in seconds
   * @type {number}
   */
  private step:number;

  /**
   * @returns {number} fps
   * @public
   */
  public getFps():number {
    return this.fps;
  }

  /**
   * @returns {number} interval
   * @public
   */
  public getInterval():number {
    return this.interval;
  }

  /**
   * @returns {number} step
   * @public
   */
  public getStep():number {
    return this.step;
  }

  /**
   * Game update loop interval id
   * @type {any}
   * @private
   */
  private intervalId:any = -1;

  /**
   * Stage loader for all of the stages
   * @type {StageLoader}
   * @private
   */
  private loader:StageLoader;

  /**
   * Constructs new CtxDrawer from the given Cavas Rendering Context
   * @param {CanvasRenderingContext2D} ctx canvas rendering context
   * @param {Example} example example to be loaded
   * @param {number} fps frames per second
   *
   * @public
   * @constructor
   */
  public constructor( ctx:CanvasRenderingContext2D, fps:number = 30 ) {
    this.ctx = ctx;
    this.fps = fps;
    this.interval = 1000 / fps; // milliseconds
    this.step = this.interval / 1000 // seconds

    this.loader = new StageLoader();

    for( const el of Object.keys( AllInteractiveEvents ) ) {
      this.ctx.canvas.addEventListener( el, this.loader.callEvent, false );
    }
  }

  /**
   * @returns {StageLoader} active stage loader
   * @public
   */
  public getLoader():StageLoader {
    return this.loader;
  }

  /**
   * Resets all of the local values to their initial state
   * Note: This method does not interfere with the game update interval
   * @public
   */
  public reset() {
    this.loader.reset();
  }

  /**
   * Starts the game and interval without changing any internal variables
   * In order to "halt" the game use {@link #stop}
   * @param {()=>void} fn function to be called in the interval
   *
   * @throws {GameError} if trying to start before clearing the interval
   * @public
   */
  public start(fn:()=>void) {
    if ( this.intervalId !== -1 )
      throw new GameError( "Cannot start already started game without calling stop() before" );

    this.intervalId = setInterval( fn, this.interval );
  }

  /**
   * Stops the game and clears interval id without changing any internal variables
   * In order to "resume" the game use {@link #start} method again
   * @throws {GameError} if trying to stop before starting the interval
   * @public
   */
  public stop() {
    if( this.intervalId === -1 )
      throw new GameError( "Cannot stop the game before calling start() before" );

    clearInterval( this.intervalId );
    this.intervalId = -1;
  }

  /**
   * Updates current drawing context
   * @public
   */
  public update() {
    this.loader.draw( this.ctx, 0, 0 );
  }
}
function AllInteractiveEvents(AllInteractiveEvents: any) {
  throw new Error("Function not implemented.");
}

