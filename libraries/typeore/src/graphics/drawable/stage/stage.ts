/**
 * stages.ts - stages interface
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { GameStateObject } from "../../../game_state";
import { Drawable } from "../drawable";
import { SupportedEventTypes } from "../interactive/interactive";

/**
 * stages interface
 * Note: all stages has to be drawable
 * @interface
 * @extends Drawable
 */
export interface Stage extends Drawable {
  /**
   * Resets all of the values to their initial state
   */
  reset():void;

  /**
   * Forwards event call to the given stage if it has given type implemented
   * @param {SupportedEventTypes} event event to be forwarded
   */
   callEvent(event:SupportedEventTypes ):void;
}

/**
 * For static implementation of stage state
 * @interface
 */
 export interface StageState {
  /**
   * Game State corresponding to this stage
   * @type {GameStateObject}
   * @readonly
   */
   readonly state:GameStateObject;
}
