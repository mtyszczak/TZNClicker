/**
 * effect.ts - contains ready to derive from interactive effect class
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

 "use strict";

import { Effects } from "../animation/animation";
import { AllEvents, AllInteractiveEvents, SupportedEventTypes } from "./interactive";

/**
 * Ready to derive from interactive effect class
 * @class
 * @extends Effects
 * @implements AllInteractiveEvents
 */
export class InteractiveEffect extends Effects implements AllInteractiveEvents {
  /**
   * Bypass typescript's weak type detection
   * @type {(event:FocusEvent)=>void}
   * @public
   * @override
   * @event
   */
  public blur?:(event:FocusEvent)=>void;

  /**
   * Adds event listener to the current class
   * Note: there can be only one listener per interactive object, so every time you call this method with the
   * same name you override the previous listener
   * @param {AllEvents} name event name
   * @param {(event:SupportedEventTypes)=>void} listener listener method
   */
  public addEventListener( name:AllEvents, listener:(event:SupportedEventTypes)=>void ) {
    this[ name ] = listener;
  }
}
