/**
 * interactive.ts - interface for all the interactives
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

 "use strict";

 /**
  * Currently supported event types. Should match all possible event types from the AllInteractiveEvents
  * @see AllInteractiveEvents
  */
export type SupportedEventTypes = Event|FocusEvent|MouseEvent|MouseEvent|KeyboardEvent;

export const IndependentEvents = [ "blur", "fullscreenchange" ] as const;
/**
 * independent events (applies to the whole drawing canvas)
 */
export type IndependentEventsType = typeof IndependentEvents[ number ];

export const MouseOverDependentEvents = [ "click", "contextmenu", "dblclick", "mousedown", "mousemove", "mouseup", "scroll" ] as const;
/**
 * Mouse over dependent events (that depend on current relative mouse position)
 */
export type MouseOverDependentEventsType = typeof MouseOverDependentEvents[ number ];

export const MouseFocusDependentEvents = [ "keydown", "keyup" ] as const;
/**
 * Mouse focus (clicked) dependent events (that depend on last mouse clicked position -
 * indicates which element should have focus / apply those events)
 */
export type MouseFocusDependentEventsType = typeof MouseFocusDependentEvents[ number ];

/**
 * All interactive events
 */
export type AllEvents = IndependentEventsType | MouseFocusDependentEventsType | MouseOverDependentEventsType;

 /**
  * All possible interactives
  * @interface
  */
export interface AllInteractiveEvents {
  // TODO: Audio and images: AudioTrackList
  /**
   * The blur event fires when an element has lost focus. (Applies only to the game container)
   * @type {(event:FocusEvent)=>void}
   * @event
   */
  blur?:(event:FocusEvent)=>void;

  /**
   * An element receives a click event when a pointing device button (such as a mouse's primary mouse button)
   * is both pressed and released while the pointer is located inside the element.
   * @type {(event:MouseEvent)=>void}
   * @event
   */
  click?:(event:MouseEvent)=>void;

  /**
   * The contextmenu event fires when the user attempts to open a context menu.
   * This event is typically triggered by clicking the right mouse button, or by pressing the context menu key.
   * @type {(event:MouseEvent)=>void}
   * @event
   */
  contextmenu?:(event:MouseEvent)=>void;

  /**
   * The dblclick event fires when a pointing device button (such as a mouse's primary button) is double-clicked;
   * that is, when it's rapidly clicked twice on a single element within a very short span of time.
   * @type {(event:MouseEvent)=>void}
   * @event
   */
  dblclick?:(event:MouseEvent)=>void;

  /**
   * The fullscreenchange event is fired immediately after an Element switches into or out of full-screen mode.
   * @type {(event:Event)=>void}
   * @event
   */
  fullscreenchange?:(event:Event)=>void;

  /**
   * The keydown event is fired when a key is pressed.
   * @type {(event:KeyboardEvent)=>void}
   * @event
   */
  keydown?:(event:KeyboardEvent)=>void;

  /**
   * The keydown event is fired when a key is released.
   * @type {(event:KeyboardEvent)=>void}
   * @event
   */
  keyup?:(event:KeyboardEvent)=>void;

  /**
   * The mousedown event is fired at an Element when a pointing device button is pressed
   * while the pointer is inside the element.
   * @type {(event:MouseEvent)=>void}
   * @event
   */
  mousedown?:(event:MouseEvent)=>void;

  /**
   * The mousemove event is fired at an element when a pointing device (usually a mouse) is moved
   * while the cursor's hotspot is inside it.
   * @type {(event:MouseEvent)=>void}
   * @event
   */
  mousemove?:(event:MouseEvent)=>void;

  /**
   * The mouseup event is fired at an Element when a button on a pointing device (such as a mouse or trackpad)
   * is released while the pointer is located inside it.
   * @type {(event:MouseEvent)=>void}
   * @event
   */
  mouseup?:(event:MouseEvent)=>void;

  /**
   * The scroll event fires an element has been scrolled.
   * @type {(event:Event)=>void}
   * @event
   */
  scroll?:(event:Event)=>void;

  // TODO: touch, drag and clipboard events
}
