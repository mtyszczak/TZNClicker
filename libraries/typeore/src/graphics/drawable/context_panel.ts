/**
 * context_panel.ts - contains classes that manage multiple drawable objects
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

import { staticAssert } from "../../../../js-helpers/src/assert";
import { Pair } from "../../../../js-helpers/src/structs";
import { AnimationEffects, applyAnimationEffects, Coordinates, Effects } from "./animation/animation";
import { Drawable } from "./drawable";
import { SupportedEventTypes, MouseOverDependentEvents, MouseFocusDependentEvents } from "./interactive/interactive";

/**
 * type of the drawables map in the ContextPanel class
 * @see ContextPanel#drawables
 */
export type ContextPanelPairType = Pair< Drawable, AnimationEffects >;

/**
 * Holds and manages multiple Drawables also being drawable
 * @class
 * @extends Effects
 * @implements Drawable
 */
export class ContextPanel extends Effects implements Drawable {

  /**
   * Array of the registered drawables
   * @type {Array< ContextPanelPairType >}
   * @private
   */
  private drawables:Array< ContextPanelPairType > = [];

  /**
   * Default constructor
   * @public
   * @constructor
   */
  public constructor() {
    super();
  }

  /**
   * object providing information about the size of an element and its position relative to the viewport
   * @type {DOMRect}
   * @public
   */
  public position: DOMRect = {} as DOMRect;

  /**
   * Accesses the top element
   * @returns {ContextPanelPairType} top element
   * @throws {AssertionError} if drawables length is equal to 0
   * @public
   */
  public top():ContextPanelPairType {
    staticAssert( this.drawables.length > 0, "Drawables array in ContextPanel is empty" );
    return this.drawables[ this.drawables.length - 1 ];
  }

  /**
   * Adds new elements to the context panel with fixed coordinates to draw
   * @param {Drawable} drawable object do be drawn
   * @param {AnimationEffects} effects effects for the drawable object to be drawn
   * @returns {number} number of elements in this context panel
   * @public
   */
  public push( drawable:Drawable, effects:AnimationEffects = {} as AnimationEffects ):number {
    return this.drawables.push( new Pair( drawable, effects ) );
  }

  /**
   * Performs the specified action for each element in an array.
   * @param {(value:ContextPanelPairType, index:number, array:ContextPanelPairType[])=>void} callbackFn
   *        A function that accepts up to three arguments. forEach calls the callbackfn function one time for each element in the array.
   * @param {any} thisArg An object to which the this keyword can refer in the callbackfn function. If thisArg is omitted, undefined is used as the this value.
   * @public
   */
  public forEach( callbackFn: (value:ContextPanelPairType, index:number, array:ContextPanelPairType[])=>void, thisArg:any ) {
    this.drawables.forEach( callbackFn, thisArg );
  }

  /**
   * @returns {number} number of all of the drawables in this panel
   */
  public length():number {
    return this.drawables.length;
  }

  /**
   * Retrieves drawable-coordinates pair with given index from this context panel
   * @param {ContextPanelPairType} index index of the element to retrieve
   * @returns {ContextPanelPairType} requested element
   * @throws {AssertionError} if index is out of range
   * @public
   */
  public get( index:number ):ContextPanelPairType {
    staticAssert( index >= 0 && index < this.drawables.length, "Requesting context panel object with index out of range", { index, drawablesLength: this.drawables.length } );
    return this.drawables[ index ];
  }

  /**
   * Pops the last element from the context panel
   * @returns {ContextPanelPairType} popped element
   * @public
   */
  public pop():ContextPanelPairType {
    return this.drawables.pop();
  }

  /**
   * Last mouse over coordinates
   * @type {Coordinates}
   * @private
   */
  private lastMouseOver :Coordinates = {x:-1,y:-1};

  /**
   * Last mouse focus coordinates
   * @type {Coordinates}
   * @private
   */
  private lastMouseFocus:Coordinates = {x:-1,y:-1};

  /**
   * Forwards event calls to the elements
   * @param {SupportedEventTypes} event event to be forwarded
   * @public
   */
  public callEvent( event:SupportedEventTypes ) {
    switch( event.type ) {
      case "mousedown":
        this.lastMouseFocus = {
          x: ( event as MouseEvent ).offsetX,
          y: ( event as MouseEvent ).offsetY
        };
        break;
      case "mouseover":
        this.lastMouseOver = {
          x: ( event as MouseEvent ).offsetX,
          y: ( event as MouseEvent ).offsetY
        };
        break;
      default:
    }

    for( const el of this.drawables ) {
      if( event.type in el ) {
        let shouldApply = false;
        if( event.type in MouseOverDependentEvents )
          shouldApply = el.first.position.contains( this.lastMouseOver ); // FIXME: Currently not working with specified values in el.second
        else if ( event.type in MouseFocusDependentEvents )
          shouldApply = el.first.position.contains( this.lastMouseFocus );
        else
          shouldApply = true;
        if( shouldApply )
          el[ event.type ]( event );
      }
    }
  }

  /**
   * Clears current context panel from all of the drawables
   * @public
   */
  public clear() {
    this.drawables = [];
  }

  /**
  * Draws this context panel at given coordinates
  * @param {CanvasRenderingContext2D} ctx rendering context
  * @param {number} x drawing x coordinates
  * @param {number} y drawing y coordinates
  * @public
  * @override
  */
  public draw(ctx: CanvasRenderingContext2D, x: number, y: number) {
    this.position.x = x + this.getRelativeX();
    this.position.y = y + this.getRelativeY();
    let maxWidth = 0;
    let maxHeight = 0;
    ctx.save();
    super.applyEffects( ctx );
    for( const drawable of this.drawables ) {
      ctx.save();
      applyAnimationEffects( ctx, drawable.second );
      drawable.first.draw( ctx, x, y );
      maxWidth = Math.max( maxWidth, drawable.first.position.x + drawable.first.position.width );
      maxHeight = Math.max( maxHeight, drawable.first.position.y + drawable.first.position.height );
      ctx.restore();
    }
    ctx.restore();
    this.position.width = this.position.x - maxWidth;
    this.position.height = this.position.y - maxHeight;
  }

}
