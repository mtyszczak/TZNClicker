/**
 * stage_loader.ts - stages container and loader
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { GameStateObject } from "../../game_state";
import { Effects } from "./animation/animation";
import { ContextPanel } from "./context_panel";
import { Drawable } from "./drawable";
import { SupportedEventTypes } from "./interactive/interactive";
import { Rectangle } from "./shape/rectangle";
import { Stage } from "./stage/stage";

/**
 * Stage map object where key is game state and the value is any object with given stage
 */
export type StageMap = {
  [ key:number ]: Stage;
};

/**
 * Stage container and loader
 * Note: Contains members from Stage interface, but it is not a standalone Stage, but their loader!
 * Intentionally implements Drawable not Stage
 * @class
 * @extends Effects
 * @implements Drawable
 */
export class StageLoader extends Effects implements Drawable {

  /**
   * All of the stages map
   * @type {StageMap}
   * @private
   */
  private stages:StageMap = {} as StageMap;

  /**
   * Current state
   * @type {GameStateObject}
   * @private
   */
  private state:GameStateObject;

  /**
   * object providing information about the size of an element and its position relative to the viewport
   * @type {DOMRect}
   * @public
   */
  public position: DOMRect = {} as DOMRect;


  /**
   * Constructs new StageLoader object and saves given GameState as a current state
   * Note: before using any Stage interface methods make sure that you added your stage to the loader
   *
   * @see #register
   * @see #load
   *
   * @public
   * @constructor
   */
  public constructor() {
    super({
      scale: { x: 1, y: 1 }
    });
  }

  /**
   * Registers given stage
   * @param {Stage} stage stage to be registered
   * @public
   */
  public register( stage:Stage ) {
    this.stages[ Object.getPrototypeOf( stage ).constructor.state.id ] = stage;
  }

  /**
   * Loads given state
   * @param {GameStateObject} state game state to be loaded
   * @public
   */
  public load( state:GameStateObject ) {
    this.state = state;
  }

  /**
   * Resets currently loaded stage to its initial value
   * @public
   */
  public reset() {
    this.stages[ this.state.id ].reset();
  }

  /**
   * Forwards event call to the given stage if it has given type implemented
   * @param {SupportedEventTypes} event event to be forwarded
   * @public
   */
  public callEvent( event:SupportedEventTypes ) {
      this.stages[ this.state.id ].callEvent( event ); // TODO: Error catching
  }

  /**
   * Draws currently loaded stage at given coordinates
   * @param {CanvasRenderingContext2D} ctx rendering context
   * @param {number} x drawing x coordinates
   * @param {number} y drawing y coordinates
   *
   * @public
   * @override
   */
  public draw(ctx: CanvasRenderingContext2D, x: number, y: number) {
    ctx.save();
    super.applyEffects( ctx );
    this.stages[ this.state.id ].draw( ctx, x, y );
    this.position = this.stages[ this.state.id ].position;
    ctx.restore();
  }

}
