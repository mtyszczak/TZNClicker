/**
 * animation.ts - animation settings and utilities
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

 "use strict";

 /**
  * Just to differ function calls and values in AnimationEffects
  * @interface
  */
export interface FunctionEffect {}

/**
 * Transform interface
 * @interface
 */
export interface Transform extends FunctionEffect {
  horizontalScale:number; // 1
  horizontalSkew:number;  // 0
  verticalSkew:number;    // 0
  verticalScale:number;   // 1
  horizontalMove:number;  // 0
  verticalMove:number;    // 0
}

/**
 * Basic Coordinates holder
 * @interface
 */
export interface Coordinates extends FunctionEffect {
  x:number;
  y:number;
}

/**
 * Basic Value holder
 * @interface
 */
export interface Value extends FunctionEffect {
  value:number;
}

/**
 * Basic Clip interface for canvas clip() method
 * @interface
 */
export interface Clip extends FunctionEffect {
  path: Path2D;
  fillRule?: CanvasFillRule;
}

/**
 * Basic line dash list holder
 * @interface
 */
export interface LineDash extends FunctionEffect {
  list:Array<number>;
}

/**
 * Basic text holder
 * @interface
 */
export interface TextEffect extends FunctionEffect {
  text:string;
}

/**
 * Animation effects
 * @interface
 */
export interface AnimationEffects {
  translate?:Coordinates;
  scale?:Coordinates;
  rotate?:Value;
  transform?:Transform;
  clip?:Clip;
  lineDash?:LineDash;
  strokeStyle?:string; // TODO: CanvasGradient|CanvasPattern
  fillStyle?:string; // Same here ^^^
  globalAlpha?:number;
  lineWidth?:number;
  lineCap?:( "butt"|"round"|"square" );
  lineJoin?:( "bevel"|"round"|"miter" );
  miterLimit?:number;
  lineDashOffset?:number;
  shadowOffsetX?:number;
  shadowOffsetY?:number;
  shadowBlur?:number;
  shadowColor?:string;
  globalCompositeOperation?:string;
  font?:string;
  textAlign?:("left"|"right"|"center"|"start"|"end");
  textBaseline?:("top"|"hanging"|"middle"|"alphabetic"|"ideographic"|"bottom");
  direction?:("ltr"|"rtl"|"inherit");
  imageSmoothingEnabled?:boolean;
  centerX?:TextEffect;
  centerY?:TextEffect;
}

 /**
  * Animation settings
  * @interface
  */
export interface AnimationSettings {
  from:AnimationEffects;
  to:AnimationEffects;
}

/**
 * Implements Animation effects and applyEffects method
 * Note: Every class extending from this class that also implements Drawable should
 * save() context, then {@link #applyEffects} and at the end restore() context
 * @class
 */
export class Effects {
  /**
   * Optional effects to be applied on this drawable
   * @type {AnimationEffects}
   * @protected
   */
   protected effects:AnimationEffects;

   /**
    * Constructs new effects class with given values
    * @param {AnimationEffects} effects effects to be applied
    * @public
    * @constructor
    */
   public constructor( effects:AnimationEffects = {} as AnimationEffects ) {
     this.effects = effects;
   }

   /**
    * Merges given effects with current effects
    * @param {AnimationEffects} effects effects to be merged
    */
   public mergeEffects( effects:AnimationEffects ) {
     this.effects = { ...this.effects, ...effects };
   }

   /**
    * @returns {AnimationEffects} currently saved effects
    */
   public getEffects():AnimationEffects {
     return this.effects;
   }

   /**
    * @returns {number} relative x coordinates basedd on transform and translate values
    */
   public getRelativeX():number {
    return typeof this.effects.transform === "undefined" ? 0 : this.effects.transform.horizontalMove
         + typeof this.effects.translate === "undefined" ? 0 : this.effects.translate.x;
   }

   /**
    * @returns {number} relative y coordinates basedd on transform and translate values
    */
   public getRelativeY():number {
    return typeof this.effects.transform === "undefined" ? 0 : this.effects.transform.verticalMove
         + typeof this.effects.translate === "undefined" ? 0 : this.effects.translate.y;
   }

   /**
    * Applies optional effects on the given rendering context
    * @param {CanvasRenderingContext2D} ctx rendering context
    * @public
    * @see #effects
    */
   public applyEffects( ctx:CanvasRenderingContext2D ) {
     applyAnimationEffects( ctx, this.effects );
   }
}

/**
 * Applies animation effects on given context
 * Note: Remember to save() before calling this function and restore() after it
 * @param {CanvasRenderingContext2D} ctx rendering context
 * @param {AnimationEffects} effects effects to be applied
 */
export const applyAnimationEffects = ( ctx:CanvasRenderingContext2D, effects:AnimationEffects ) => {
  for( const [ key, value ] of Object.entries( effects ) ) {
    if( typeof value === "object" )
      ctx[ key ]( ...Object.values( value ) );
    else
      ctx[ key ] = value;
  }
};

declare global {
  /**
   * Extends existing 2d canvas rendering context
   * @interface
   */
  interface CanvasRenderingContext2D {
    /**
     * Applies x translation based on the length of the text and font type
     * Note: Remember to set proper font in the same AnimationEffects object
     * or before calling this in order to calculate the proper translation coordinates
     * @param {string} text text to be displayed
     */
    centerX(text:string):void;
    /**
     * Applies y translation based on the length of the text and font type
     * Note: Remember to set proper font in the same AnimationEffects object
     * or before calling this in order to calculate the proper translation coordinates
     * @param {string} text text to be displayed
     */
    centerY(text:string):void;
  }
}

CanvasRenderingContext2D.prototype.centerX = function ( text:string ) {
  const currentTransform = this.getTransform();
  const xOffset = currentTransform.e;

  const fontSizeHorizontalMetrics = this.measureText( text ),
        fontSizeHorizontal = fontSizeHorizontalMetrics.actualBoundingBoxLeft + fontSizeHorizontalMetrics.actualBoundingBoxRight;

  const relativeCenter = ( this.canvas.clientWidth - xOffset ) / 2;

  this.translate( relativeCenter - ( fontSizeHorizontal / 2 ), 0 );
};

CanvasRenderingContext2D.prototype.centerY = function ( text:string ) {
  const currentTransform = this.getTransform();
  const yOffset = currentTransform.f;

  const fontSizeVerticalMetrics = this.measureText( text ),
        fontSizeVertical = fontSizeVerticalMetrics.actualBoundingBoxAscent + fontSizeVerticalMetrics.actualBoundingBoxDescent;

  const relativeCenter = ( this.canvas.clientHeight - yOffset ) / 2;

  this.translate( 0, relativeCenter - ( fontSizeVertical / 2 ) );
};
