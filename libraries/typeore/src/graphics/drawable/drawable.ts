/**
 * drawable.ts - interface for the every drawable object
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { Coordinates } from "./animation/animation";

/**
 * Drawable interface for the ctx drawer to draw
 * @interface
 */
export interface Drawable {

  /**
   * object providing information about the size of an element and its position relative to the viewport
   * @type {DOMRect}
   */
  position:DOMRect;

  /**
   * Universal draw method
   * @param {CanvasRenderingContext2D} ctx rendering context
   * @param {number} x drawing x coordinates
   * @param {number} y drawing y coordinates
   */
  draw( ctx:CanvasRenderingContext2D, x:number, y:number ):void;
}

/**
 * Basic Padding interface
 */
 export interface Padding {
  top:number;
  bottom:number;
  left:number;
  right:number;
}

declare global {
  /**
   * Extends existing DOMRect interface
   * @interface
   */
  interface DOMRect {
    /**
     * Checks if this DOMRect contains given point with specified coordinates
     * @param {Coordinates} point x and y coordinates to be checked
     */
    contains(point:Coordinates):boolean;
  }
}

DOMRect.prototype.contains = function( point:Coordinates ):boolean {
  return this.x <= point.x && point.x <= this.x + this.width &&
         this.y <= point.y && point.y <= this.y + this.height;
}
