/**
 * text.ts - interface for the basic text
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

 "use strict";

import { Effects } from "../animation/animation";
import { Drawable, Padding } from "../drawable";

/**
 * Text style interface for specifying styles
 * @interface
 */
export interface TextStyle {
  fillStyle?:string;
  font?:string;
  padding?:Padding;
}

 /**
  * Basic Text object
  * In constructor you pass text to be displayed and its font and then in draw method you pass the x and y coordinates for drawing
  * @class
  * @extends Effects
  * @implements Drawable
  */
 export class Text extends Effects implements Drawable {

  /**
   * text to be displayed
   * @type {string}
   * @private
   */
  private textV:string;

  /**
   * object providing information about the size of an element and its position relative to the viewport
   * @type {DOMRect}
   * @public
   */
  public position: DOMRect = {} as DOMRect;

   /**
    * Constructs new Text with fixed text and style
    * @param {number} text text to be displayed
    *
    * @see TextStyle
    * @public
    * @constructor
    */
   public constructor( text:string ) {
     super();
     this.textV = text;
   }

   /**
    * Sets or retrieves the text
    * @param {string|null} value string to be set for text or null if you want to retrieve the value (defaults to null)
    * @returns {string} current text
    * @public
    */
   public text( value:string|null = null ):string {
     return ( value !== null ? this.textV = value : this.textV );
   }

   /**
    * Draws a text at given coordinates
    * @param {CanvasRenderingContext2D} ctx rendering context
    * @param {number} x drawing x coordinates
    * @param {number} y drawing y coordinates
    *
    * @public
    */
   public draw(ctx: CanvasRenderingContext2D, x: number, y: number) {
    this.position.x = x + this.getRelativeX();
    this.position.y = y + this.getRelativeY();
    const txt = ctx.measureText( this.textV );
    this.position.width = txt.actualBoundingBoxLeft + txt.actualBoundingBoxRight;
    this.position.height = txt.actualBoundingBoxDescent + txt.actualBoundingBoxAscent;
    ctx.save();
    super.applyEffects( ctx );

    ctx.fillText( this.textV, x, y );

    ctx.restore();
   }
 }
