/**
 * rectangle.ts - interface for the basic rectangle
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { Drawable } from "../drawable";
import { InteractiveEffect } from "../interactive/effect";

 /**
  * Basic Rectangle object
  * In constructor you pass width and height and then in draw method you pass the x and y coordinates for drawing
  * @class
  * @extends InteractiveEffect
  * @implements Drawable
  */
 export class Rectangle extends InteractiveEffect implements Drawable {

  /**
   * width value
   * @type {number}
   * @private
   */
  private widthV:number;

  /**
   * height value
   * @type {number}
   * @private
   */
  private heightV:number;

  /**
   * object providing information about the size of an element and its position relative to the viewport
   * @type {DOMRect}
   * @public
   */
  public position: DOMRect = {} as DOMRect;

   /**
    * Constructs new Stroke with fixed width and height
    * @param {number} width
    * @param {number} height
    *
    * @see #draw
    * @public
    * @constructor
    */
   public constructor( width:number, height:number ) {
     super();
     this.widthV = width;
     this.heightV = height;
   }

   /**
    * Sets or retrieves the width
    * @param {number|null} value number to be set for width or null if you want to retrieve the value (defaults to null)
    * @returns {number} current width
    * @public
    */
   public width( value:number|null = null ):number {
     return ( value !== null ? this.widthV = value : this.widthV );
   }

   /**
    * Sets or retrieves the height
    * @param {number|null} value number to be set for height or null if you want to retrieve the value (defaults to null)
    * @returns {number} current height
    * @public
    */
   public height( value:number|null = null ):number {
     return ( value !== null ? this.heightV = value : this.heightV );
   }

   /**
    * Draws a rectangle at given coordinates
    * @param {CanvasRenderingContext2D} ctx rendering context
    * @param {number} x drawing x coordinates
    * @param {number} y drawing y coordinates
    *
    * @public
    */
   public draw(ctx: CanvasRenderingContext2D, x: number, y: number) {
     this.position.x = x + this.getRelativeX();
     this.position.y = y + this.getRelativeY();
     this.position.width = this.widthV;
     this.position.height = this.heightV;
    ctx.save();
    super.applyEffects( ctx );

    ctx.fillRect( x, y, this.widthV, this.heightV );

    ctx.restore();
   }
 }
