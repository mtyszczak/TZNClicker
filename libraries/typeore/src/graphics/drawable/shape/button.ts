/**
 * button.ts - interface for basic button
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

 "use strict";

import { Drawable } from "../drawable";
import { Text } from "./text";
import { InteractiveEffect } from "../interactive/effect";
import { TextStyle } from "./text";

/**
 * Button style interface for specifying styles
 * @interface
 */
 export interface ButtonStyle {
  text?:TextStyle;
  borderColor?:string;
  borderWidth?:number;
  borderRadius?:number;
  backgroundColor?:string;
}

 /**
  * Interactive button obejct
  * In constructor you pass text for the button and its style
  * @class
  * @extends InteractiveEffect
  * @implements Drawable
  */
  export class Button extends InteractiveEffect implements Drawable {

    /**
     * text to be displayed
     * @type {Text}
     * @private
     */
     private textV:Text;

    /**
     * button style
     * @type {ButtonStyle}
     * @private
     */
    private styleV:ButtonStyle;

    /**
     * object providing information about the size of an element and its position relative to the viewport
     * @type {DOMRect}
     * @public
     */
    public position: DOMRect = {} as DOMRect;

    /**
     * Constructs new Button with fixed text and style
     * @param {Text} text text to be displayed
     * @param {ButtonStyle} style button style
     *
     * @see ButtonStyle
     * @public
     * @constructor
     */
    public constructor( text:Text, style:ButtonStyle = {} as ButtonStyle ) {
      super();
      this.textV = text;
      this.styleV = style;
    }

    /**
     * Sets or retrieves the text
     * @param {Text|null} value string to be set for text or null if you want to retrieve the value (defaults to null)
     * @returns {Text} current text
     * @public
     */
    public text( value:Text|null = null ):Text {
      return ( value !== null ? this.textV = value : this.textV );
    }

    /**
     * Sets or retrieves the style value
     * @param {ButtonStyle|null} value number to be set for style or null if you want to retrieve the value (defaults to null)
     * @returns {ButtonStyle} current style
     * @public
     */
    public style( value:ButtonStyle|null = null ):ButtonStyle {
      return ( value !== null ? this.styleV = value : this.styleV );
    }

    /**
     * Draws a text at given coordinates
     * @param {CanvasRenderingContext2D} ctx rendering context
     * @param {number} x drawing x coordinates
     * @param {number} y drawing y coordinates
     *
     * @public
     */
    public draw(ctx: CanvasRenderingContext2D, x: number, y: number) {
      ctx.save();
      super.applyEffects( ctx );

      // TODO: draw

      ctx.restore();
      this.position.x = x + this.getRelativeX() + this.textV.position.x;
      this.position.y = y + this.getRelativeY() + this.textV.position.y;
      this.position.width = this.styleV.text.padding.left + this.styleV.text.padding.right
                          + this.styleV.borderWidth * 2 + this.textV.position.width;
      this.position.height = this.styleV.text.padding.top + this.styleV.text.padding.bottom
                           + this.styleV.borderWidth * 2 + this.textV.position.height;
    }
  }
