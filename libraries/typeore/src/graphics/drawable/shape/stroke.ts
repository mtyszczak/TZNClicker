/**
 * stroke.ts - interface for the basic stroke
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { Drawable } from "../drawable";
import { InteractiveEffect } from "../interactive/effect";

/**
 * Basic stroke object
 * In constructor you pass first x and y coordinates and then in draw method you pass the second x and y
 * @class
 * @extends InteractiveEffect
  * @implements Drawable
 */
export class Stroke extends InteractiveEffect implements Drawable {

  /**
   * first x coordinates
   * @type {number}
   * @private
   */
  private x:number;

  /**
   * first y coordinates
   * @type {number}
   * @private
   */
  private y:number;

  /**
   * object providing information about the size of an element and its position relative to the viewport
   * @type {DOMRect}
   * @public
   */
  public position: DOMRect = {} as DOMRect;

  /**
   * Constructs new Stroke with fixed to x and y coordinates
   * @param {number} xTo first x coordinates
   * @param {number} yTo first y coordinates
   *
   * @see #draw
   * @public
   * @constructor
   */
  public constructor( xTo:number, yTo:number ) {
    super();
    this.x = xTo;
    this.y = yTo;
  }

  /**
   * Sets or retrieves the x value
   * @param {number|null} value number to be set for x or null if you want to retrieve the value (defaults to null)
   * @returns {number} current x
   * @public
   */
  public xTo( value:number|null = null ):number {
    return ( value !== null ? this.x = value : this.x );
  }

  /**
   * Sets or retrieves the y value
   * @param {number|null} value number to be set for y or null if you want to retrieve the value (defaults to null)
   * @returns {number} current y
   * @public
   */
  public yTo( value:number|null = null ):number {
    return ( value !== null ? this.y = value : this.y );
  }

  /**
   * Draws a stroke to given coordinates
   * @param {CanvasRenderingContext2D} ctx rendering context
   * @param {number} x drawing x coordinates
   * @param {number} y drawing y coordinates
   *
   * @public
   */
  public draw(ctx: CanvasRenderingContext2D, x: number, y: number) {
    const xRelative = x + this.getRelativeX();
    const yRelative = y + this.getRelativeY();
    this.position.x = Math.min( this.x, xRelative );
    this.position.y = Math.min( this.y, yRelative );
    this.position.width = Math.max( this.x, xRelative );
    this.position.height = Math.max( this.y, yRelative );
    ctx.save();
    super.applyEffects( ctx );

    ctx.beginPath();
    ctx.moveTo( x, y );
    ctx.lineTo( this.x, this.y );
    ctx.stroke();

    ctx.restore();
  }
}
