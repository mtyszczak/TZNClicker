/**
 * ui.ts - Themes and colors data
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { staticAssert } from "../../../js-helpers/src/assert";

/**
 * Contains all of the font and display settings
 * @interface
 */
export interface Dimensions {
  strokeSize: number;
  fontSize:number;
  /**
   * In-game font family
   */
  fontFamily1:string;
  /**
   * Menu, options etc. font family
   */
  fontFamily2:string;
  leftMarginSize:number;
  rightMarginSize:number;
  topMarginSize:number;
  bottomMarginSize:number;
  tabIndentation:number;
}

/**
 * Using Theme as an interface ensures the programmer that he has not ommited any value
 * and every Theme will be compatible with other Themes
 * @interface
 */
export interface Theme {
  /**
   * Background color for the game
   * @type {string}
   */
  background:string;

  /**
   * Secondary color (for example not highlighted lines)
   * @type {string}
   */
  secondary:string;

  /**
   * Foreground color (for example text and highlighted lines)
   * @type {string}
   */
  foreground:string;

  /**
   * Font and display related values
   * @type {Dimensions}
   */
  dimens:Dimensions;
};

/**
 * Default theme to be used in app
 * @type {Theme}
 * @constant
 */
export const defaultTheme:Theme = {
  background: "transparent",
  secondary: "gray",
  foreground: "black",

  dimens: {
    strokeSize: 6,
    fontSize: 14,
    fontFamily1: "monospace", // TODO: Font array or font name change
    fontFamily2: "Arial",
    leftMarginSize: 12,
    rightMarginSize: 12,
    topMarginSize: 3,
    bottomMarginSize: 3,
    tabIndentation: 2
  }
};

/**
 * Orientation of the line to be used in lineSize method to retrieve line props
 * @enum
 */
export enum LineOrientation {
  VERTICAL,
  HORIZONTAL
};

/**
 * Ensures that theme values are valid
 * @param {Theme} data theme to be validated
 * @returns {Theme} unchaged data
 *
 * @throws {AssertionError} when one of the values is invalid
 * @constant
 */
const checkTheme = ( data:Theme ):Theme => {
  staticAssert( data.dimens.tabIndentation > 0, "Tab indentation cannot be less than 1", { "tabIndentation": data.dimens.tabIndentation } );
  staticAssert( data.dimens.fontSize > 0, "Font size cannot be less than 1", { "fontSize": data.dimens.fontSize } );

  // TODO: maybe require all of the playground values to be in hex and check them here using js-helpers::graphics::Color
  return data;
};

/**
 * User interface related settings and methods
 * @class
 */
export class UI {
  /**
   * Current theme
   * @type {Theme}
   * @private
   * @static
   */
  private static current:Theme = checkTheme( defaultTheme );

  /**
   * Current custom dadta
   * @type {object}
   * @private
   * @static
   */
  private static custom:object = {};

  /**
   * Returns line size (vertical or horizontal) with or without margins based on the current theme dimens settings
   * @param {LineOrientation} orientation orientation type
   * @param {boolean} includeMargins should include proper margin sizes from the current theme
   * @returns {number} requested value in pixels
   *
   * @see Dimensions
   * @see LineOrientation
   *
   * @public
   * @static
   */
  public static lineSize( orientation:LineOrientation, includeMargins:boolean = true ):number {
    let sizeMultiplier = orientation === LineOrientation.HORIZONTAL ? 0.6 : 1;
    let size = this.current.dimens.fontSize * sizeMultiplier;
    if( includeMargins ) {
      switch( orientation ) {
        case LineOrientation.VERTICAL:
          size += this.current.dimens.topMarginSize + this.current.dimens.bottomMarginSize;
          break;
        case LineOrientation.HORIZONTAL:
          size += this.current.dimens.leftMarginSize + this.current.dimens.rightMarginSize;
          break;
        default:
      }
    }
    return size;
  }

  /**
   * Creates new ui object
   *
   * There can be only 1 ui object in the whole game
   * @private
   * @constructor
   */
  private constructor() {}

  /**
   * Sets or retrieves current theme
   * @param {Theme|null} data theme to be set (null if you just want to retrieve the Theme)
   * @returns {Theme} current theme
   *
   * @see Theme for more information about the settings layout
   *
   * @public
   * @static
   */
  public static currentTheme( data:Theme|null = null ):Theme {
    return ( data ? this.current = checkTheme( data ) : this.current );
  }

  /**
   * Sets or retrieves custom theme data
   * @param {object|null} data custom user data to be set (null if you just want to retrieve the data)
   * @returns {object} current custom data
   *
   * @public
   * @static
   */
  public static customData< T extends object >( data:object|null = null ):any {
    return ( data ? this.custom = data : this.custom ) as T;
  }
};
