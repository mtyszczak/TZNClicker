/**
 * speed.ts - basic speed wrapper
 *
 * @version 0.1
 * @author Mateusz Tyszczak
 * @license GPLv3
 */

"use strict";

import { staticAssert } from "../../../js-helpers/src/assert";

/**
 * speed values wrapper
 * @class
 */
export class Speed {
  /**
   * Current speed (as floating point 0-1)
   * @type {number}
   * @private
   */
  private speed:number = 0;

  /**
  * multiplier for reset
  * @type {number}
  * @private
  */
  private multiplier:number = 0;

  /**
   * Constructs new speed object
   * @param {number} speed speed value
   * @param {number} multiplier multiplier value
   *
   * @throws {AssertionError} if at least one of the values is invalid (e.g. speed equal to 0 or multiplier less than 0)
   *
   * @constructor
   * @public
   */
  public constructor( speed:number, multiplier:number = 1.0 ) {
    this.adjustSpeed( speed );
    this.adjustMultiplier( multiplier );
  }

  /**
   * @returns {number} current speed
   * @public
   */
    public getSpeed():number {
    return this.speed;
  }

  /**
   * @returns {number} current multiplier
   * @public
   */
    public getMultiplier():number {
    return this.multiplier;
  }

  /**
   * calculates the real speed from equation: multiplier / speed
   * @returns {number} the real auto generated from speed and multiplier speed value
   * @public
   */
  public get():number {
    return this.multiplier / this.speed;
  }

  /**
   * Adjust the game speed by the given value
   * @param {number} by value
   * @public
   *
   * @throws {AssertionError} if trying to decrease game speed to a negative value
   */
    public adjustSpeed( by:number ) {
    staticAssert( this.speed + by > 0, "Speed cannot be decreased to a value less than or equal to zero. For that purpose use multiplier", { speed: this.speed, by } );
    this.speed += by;
  }

  /**
   * Adjust the game speed by the given value
   * @param {number} by value
   * @public
   *
   * @throws {AssertionError} if trying to decrease game speed to a negative value
   */
    public adjustMultiplier( by:number ) {
    staticAssert( this.multiplier + by >= 0, "multiplier cannot be decreased to a negative value", { multiplier: this.multiplier, by } );
    this.multiplier += by;
  }
}
