# typeore

typeore is a simple game engine written fully in TypeScript

## Features
- Events handling
- Scalable graphics
- Custom user interface (includes universal themes and custom data loading with auto type detecting)
- Universal Drawable interface wih draw method (recursive drawing)
- Basic shapes
- Stages loading
- Animations and effects

## LICENSE
Licensed under the GNU GPL v3
