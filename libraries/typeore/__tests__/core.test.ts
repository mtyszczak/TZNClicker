"use strict";

import { GameCore } from '../src/core';

describe("TZNClicker library", () => {

  const core = new GameCore();

  test("game core version check", () => {

    expect( core.getVersion() ).toBe( "0.1.0" );

  });
});
