# libraries

## Initializing
To initialize libraries run: `git submodule update --init --recursive --progress`

## List of the project libraries / dependencies
|  **Name**   |              **Description**                   | **Since** |    **Author**    | **License** |
|-------------|------------------------------------------------|-----------|------------------|-------------|
|   typeore   | simple game engine written fully in TypeScript |    0.1    | Mateusz Tyszczak | GNU GPL v3  |
| js-helpers  |    Complete tool set of JavaScript helpers     |    0.1    | Mateusz Tyszczak | GNU GPL v3  |