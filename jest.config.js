module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  rootDir: 'libraries/TZNClicker',
  testMatch: ['<rootDir>/__tests__/*.test.ts'],
};